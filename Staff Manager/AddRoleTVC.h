//
//  AddRoleTVC.h
//  Staff Manager
//
//  Created by Michael Peacock on 07/08/2012.
//  Copyright (c) 2012 CentralApps. All rights reserved.
//
#import "Role.h" // access the role model
@class AddRoleTVC;
@protocol AddRoleTVCDelegate
- (void)theSaveButtonOnTheAddRoleTVCWasTapped:(AddRoleTVC *)controller;
@end

@interface AddRoleTVC : UITableViewController
@property (nonatomic, weak) id <AddRoleTVCDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITextField *roleNameTextField;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)save:(id)sender;

@end
