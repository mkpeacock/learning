#import "RoleDetailTVC.h"

@implementation RoleDetailTVC
@synthesize delegate;
@synthesize role = _role;
@synthesize roleNameTextField;
@synthesize managedObjectContext = __managedObjectContext;

- (void)viewDidLoad
{
    NSLog(@"Setting the value of fields in this statc table to that of the passed Role");
    self.roleNameTextField.text = self.role.name;
    [super viewDidLoad];
}

- (void)viewDidUnload
{
    [self setRoleNameTextField:nil];
    [super viewDidUnload];
}

- (IBAction)save:(id)sender
{
    NSLog(@"Telling the RoleDetailTVC Delegate that Save was tapped on the RoleDetailTVC");
    [self.role setName:roleNameTextField.text];
    [self.managedObjectContext save:nil]; // write to the database
    [self.delegate theSaveButtonOnTheRoleDetailTVCWasTapped:self];
}
@end