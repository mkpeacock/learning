//
//  Role.h
//  Staff Manager
//
//  Created by Michael Peacock on 09/08/2012.
//  Copyright (c) 2012 CentralApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Role : NSManagedObject

@property (nonatomic, retain) NSString * name;

@end
