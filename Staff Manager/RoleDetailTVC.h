//
//  AddRoleTVC.h
//  Staff Manager
//
//  Created by Michael Peacock on 07/08/2012.
//  Copyright (c) 2012 CentralApps. All rights reserved.
//
#import "Role.h" // access the role model
@class RoleDetailTVC;
@protocol RoleDetailTVCDelegate
- (void)theSaveButtonOnTheRoleDetailTVCWasTapped:(RoleDetailTVC *)controller;
@end

@interface RoleDetailTVC : UITableViewController
@property (nonatomic, weak) id <RoleDetailTVCDelegate> delegate;
@property (strong, nonatomic) Role *role;
@property (strong, nonatomic) IBOutlet UITextField *roleNameTextField;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)save:(id)sender;

@end
