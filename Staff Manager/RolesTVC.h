//
//  RolesTVC.h
//  Staff Manager
//
//  Created by Michael Peacock on 07/08/2012.
//  Copyright (c) 2012 CentralApps. All rights reserved.
//

#import "AddRoleTVC.h" // so this class can be a AddRoleTVCDelegate
#import "RoleDetailTVC.h" // so this class can be a RoleDetailTVCDelegate
#import "Role.h" // access the role model
#import "CoreDataTableViewController.h" // so we can fetch
@interface RolesTVC : CoreDataTableViewController <AddRoleTVCDelegate, RoleDetailTVCDelegate>
@property (strong, nonatomic) Role *selectedRole;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@end
