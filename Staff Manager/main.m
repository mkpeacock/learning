//
//  main.m
//  Staff Manager
//
//  Created by Michael Peacock on 07/08/2012.
//  Copyright (c) 2012 CentralApps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
